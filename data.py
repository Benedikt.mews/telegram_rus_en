# me
api_id = 10235207
api_hash = "a567ad300e5af5cb6da718a624b86605"
bot_token = '12345:0123456789abcdef0123456789abcdef'
bot_token_2 = '5125646240:AAF4CHezFpUpAlsEN0tOX5X4FU9prma3mH0'
phone_number = '+4917643297994'
pw = '12290d18eat'
name = 'anon2'
chat = 1001069772626 #?
chat_id_me = 880568379

languages = ["en", "de"]

#help
chat_id_yandex_translate = 104784211
chat_id_ukraine_message_id =49446

# target channels
target_aavst = -1001619765008
target_meduza = -1001514498555
target_kharkov = -1001743421565
target_redakciya = -1001787222108
target_ukrainenow = -1001416918985
target_test = -1001566664132

channels = {
    "aavst":{
        "origin_id": -1001682735731,
        "target_id": target_aavst,
        "about": "Russian journalist Alexei Venediktov of the recently closed Echo of Moscow radio https://t.me/aavst2022 https://en.wikipedia.org/wiki/Alexei_Venediktov",
        "active": True
    },
    "hueviykharkov": {
        "origin_id": -1001149657308,
        "target_id": target_kharkov,
        "about": "'Shitty Kharkov', a channel located in the east Ukrainian city Kharkov and documenting the war live on location https://t.me/hueviykharkov https://www.instagram.com/hueviy.kharkov/?hl=en https://twitter.com/h_kharkov?lang=en",
        "active": True
    },
    "redakciya": {
        "origin_id": -1001429590454,
        "target_id": target_redakciya,
        "about": "Russian journalist Alexey Pivovarov https://t.me/redakciya_channel https://en.wikipedia.org/wiki/Alexey_Pivovarov https://www.rtvi.com/ https://twitter.com/pivo_varov/",
        "active": True
    },
    "meduza": {
        "origin_id": -1001036240821,
        "target_id": target_meduza,
        "about": "Latvia-based independent news website. Also publishes in English https://t.me/meduzalive https://meduza.io/en https://en.wikipedia.org/wiki/Meduza",
        "active": True
    },
    "ukrainenow_rus": {
        "origin_id": -1001280273449,
        "target_id": target_ukrainenow,
        "about": "Channel managed by the Ukrainian government, in Russian language https://t.me/UkraineNow and in English https://t.me/ukrainenowenglish",
        "active": True
    },
    "ukrainenow_en": {
        "origin_id": -1001715327440,
        "target_id": chat_id_me,
        "about": "Channel managed by the Ukrainian government, in Russian language https://t.me/UkraineNow and in English https://t.me/ukrainenowenglish",
        "active": False
    },
    "pravdagerashchenko_rus": {
        "origin_id": -1001722167948,
        "target_id": chat_id_me,
        "about": "Anton Gerashchenko Ukrianian advisor: https://t.me/pravdaGerashchenko_en https://en.wikipedia.org/wiki/Anton_Herashchenko",
        "active": False
    },
    "pravdagerashchenko_en": {
        "origin_id": -1001783484530,
        "target_id": chat_id_me,
        "about": "Anton Gerashchenko Ukrianian advisor: https://t.me/pravdaGerashchenko_en https://en.wikipedia.org/wiki/Anton_Herashchenko",
        "active": False
    },
    "intelslava": {
        "origin_id": -1001310984791,
        "target_id": chat_id_me,
        "about": "Pro-Russian pro-war channel in English language",
        "active": False
    },
    "myself": {
        "origin_id": chat_id_me,
        "target_id": target_test,
        "about": "For testing and debugging purposes",
        "active": True
    },
}