# todo
- website/bot/something to for info and linking of channels
- use other phone number
- make open source, hide api id
- have code run online/on phone
- other targets like twitter/instagram

# done:
- different channels with introduction etc
- async?
- media: photo/videos
- receive message from channel, pyrogram: done
- translation, deep_translator: done
- forward message to bot, pyrogram: done 
- let bot post, telebot: done

# remember errors:
- telebot bug if file is also called telebot.py
- pip doesnt install into conda
- bot cant receive message from channel
- telethon some prob with login if two factor auth is activated