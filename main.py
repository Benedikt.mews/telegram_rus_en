from data import *
from deep_translator import GoogleTranslator
import asyncio
from pyrogram import Client, emoji, filters
from datetime import datetime

# import telebot
#
# bot = telebot.TeleBot(bot_token_2) # You can set parse_mode by default. HTML or MARKDOWN
#
# @bot.message_handler(commands=['start', 'help'])
# def send_welcome(message):
# 	bot.reply_to(message, "Howdy, how are you doing?")
#
# @bot.message_handler(func=lambda m: True)
# def echo_all(message):
# 	bot.send_message(chat_id_mychannel_aavst, GoogleTranslator(source='ru', target='en').translate(message.text))
#
# bot.infinity_polling()

app = Client("my_account", api_id, api_hash)
active_origins = [channel_data["origin_id"] for channel, channel_data in channels.items() if channel_data["active"] is True]


def get_about(message):
    # about
    if message.sender_chat:
        print(str(message.sender_chat["id"]))
        sender = str(message.sender_chat["username"])
    else:
        sender = "dunno"
    time = str(datetime.utcfromtimestamp(message.date).strftime('%Y-%m-%d %H:%M:%S'))
    about_message = "from " + sender + " at " + str(time) + ":\n___\n"
    print(about_message)
    return about_message


def get_translation(text):
    print(text)
    if text is None:
        return ""
    else:
        return GoogleTranslator(source='ru', target='en').translate(text)


def get_target(message):
    print(message.sender_chat)
    if message.sender_chat is None:
        print("sent myself, ")
        print(message.sender_chat)
        return target_test
    for channel, channel_data in channels.items():
        if channel_data["origin_id"] == message.sender_chat["id"]:
            return channel_data["target_id"]
    print("ERROR no target found")
    return target_test


@app.on_message(filters.chat(active_origins) & filters.text)
async def translate_forward_text(client, message):
    print("handle text")
    new_message = get_about(message) + get_translation(message.text) + "\n___"
    await app.send_message(get_target(message), new_message)


@app.on_message(filters.chat(active_origins) & filters.photo)
async def translate_forward_photo(client, message):
    print("photo: " + str(type(message.photo)))

    await app.send_photo(chat_id=get_target(message), photo=message.photo["file_id"], caption=get_about(message) + get_translation(message.caption) + "\n'")


@app.on_message(filters.chat(active_origins) & filters.video)
async def translate_forward_video(client, message):
    print("video:")
    await app.send_video(chat_id=get_target(message), video=message.video["file_id"], caption=get_about(message) + get_translation(message.caption) + "\n'")


app.run()
